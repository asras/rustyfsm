# Finite State Machines experiments in Rust


## turnstile.rs

This is the classic and basic example of a finite state machine that you see when you start reading about it. It is an FSM with two states: locked and unlocked, and two inputs/messages/signals: coin and push.

The FSM can be represented by the following transition table:

| Input \ State | Locked | Unlocked |
| -----| ---------| ---------|
| Coin | Unlocked | Unlocked |
| Push | Locked | Locked |

turnstile.rs contains a basic implementation of this system.

## advanced_turnstile.rs

This contains an implementation that is equivalent to turnstile.rs but it uses a macro that allows one to more easily implement general FSMs.


## advanced2_turnstile.rs

Another equivalent implementation that uses procedural macros that can be found in the (sub-)crate 'helpermacro'.


## datareader.rs

This file contains an FSM implementation for a part of the AOC 2021 day 16 problem. The problem concerns reading binary packets and reading such streams can readily be implemented as an FSM.


## advanced_datareader.rs

Implements the full functionality for the AOC 2021 day 16 problem.

