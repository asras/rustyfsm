#![feature(proc_macro_quote)]
extern crate proc_macro;
use proc_macro::quote;
use proc_macro::{TokenStream, TokenTree};

#[proc_macro]
pub fn make_answer(_item: TokenStream) -> TokenStream {
    "fn answer() -> u32 { 42 }".parse().unwrap()
}

#[proc_macro]
pub fn make_fsm(spec: TokenStream) -> TokenStream {
    
    let mut result = quote! {};
    let tokens: Vec<TokenTree> = spec.into_iter().collect();
    let mut identifier: Vec<TokenTree> = vec![];
    let mut idx = 0;
    for token in &tokens {
        if token.to_string() == "-" {
            idx += 1;
            break;
        }
        identifier.push(token.clone());
        idx += 1;
    }
    let identifier = TokenStream::from_iter(identifier);

    let mut input: Vec<TokenTree> = vec![];
    for token in &tokens[idx..] {
        if token.to_string() == "-" {
            idx += 1;
            break;
        }
        input.push(token.clone());
        idx += 1;
    }
    let input = TokenStream::from_iter(input);
    
    let tokens = &tokens[idx..];
    let len = tokens.len();
    assert_eq!(len % 3, 0);
    for i in 0..(len / 3) {
        let item1 = tokens[3 * i].clone();
        let item2 = tokens[3 * i + 1].clone();
        let item3 = tokens[3 * i + 2].clone();

        // println!("Item1: {}", item1);
        // println!("Item2: {}", item2);
        // println!("Item3: {}", item3);
        result.extend::<TokenStream>({
            let part = quote! {
            ($item1, $item2) => $identifier = $item3,
            };
            part.into()
        });
    }
    let result = quote! {
        match ($identifier, $input) {
            $result
        }
    };
    result.into()
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
