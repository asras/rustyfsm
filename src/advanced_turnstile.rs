#[derive(PartialEq, Debug, Copy, Clone)]
pub enum State {
    Locked,
    Unlocked,
}
use State::*;
#[derive(Debug, Copy, Clone)]
pub enum Input {
    Coin,
    Push,
}
use Input::*;

// The FSM in this simple example keeps only one
// piece of data: its current state
pub struct FSM(State);


// To make it somewhat easier to define complicated FSMs we can make a macro
// to fill out the state transitions
macro_rules! define_fsm {
    ($state:expr, $input:expr, $(($start:ident, $signal:ident, $end:ident)),*) => {
        match ($state, $input) {
            $(
                ($start, $signal) => $state = $end,
            )*
        }
    }

}

impl FSM {
    fn new() -> Self {
        Self(Locked)
    }

    fn receive(self: &mut Self, input: Input) {
        define_fsm!(self.0, input,
                    (Locked, Coin, Unlocked),
                    (Unlocked, Coin, Unlocked),
                    (Locked, Push, Locked),
                    (Unlocked, Push, Locked));
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_fsm() {
        let fsm = FSM::new();
        assert_eq!(fsm.0, Locked);
    }

    #[test]
    fn single_coin() {
        let mut fsm = FSM::new();
        fsm.receive(Coin);
        assert_eq!(fsm.0, Unlocked);
    }

    #[test]
    fn multiple_coins() {
        let mut fsm = FSM::new();
        for _i in 0..10 {
            fsm.receive(Coin);
        }
        assert_eq!(fsm.0, Unlocked);
    }
    #[test]
    fn coin_then_push() {
        let mut fsm = FSM::new();
        fsm.receive(Coin);
        fsm.receive(Push);
        assert_eq!(fsm.0, Locked);
    }
}
