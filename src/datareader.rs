// See https://adventofcode.com/2021/day/16 for context
// We implement a (generalized) FSM for parsing the binary data strings
// in the AOC problem

fn read_data() -> String {
    let contents = std::fs::read_to_string("input.txt").expect("Couldnt read file");

    let mut bit_contents = "".to_owned();

    for ch in contents.chars() {
        let charnum = ch as u8 - '0' as u8;
        bit_contents.push_str(&decimal_to_binary(charnum));
    }

    bit_contents
}

fn decimal_to_binary(num: u8) -> String {
    let mut result = "".to_string();

    for i in 0..4 {
        if ((num >> i) & 1) != 0 {
            result.push_str("1");
        } else {
            result.push_str("0");
        }
    }

    result.chars().rev().collect()
}

#[derive(Copy, Clone, Debug)]
pub enum State {
    ReadingVersion,
    ReadingTypeId,
    ReadingNumber,
    ReadingLastNumber,
    Done,
}
use State::*;

#[derive(Copy, Clone, Debug)]
pub struct Input(char);

// In this case the FSM is more complicated: it also has associated data.
// This probably means it is not strictly speaking an FSM but rather a
// generalization.
pub struct FSMDataReader {
    state: State,
    pointer: usize,
    current: String,

    packet_version: u64,
    packet_typeid: u64,
    packet: u64,
}

impl FSMDataReader {
    fn new() -> Self {
        Self {
            state: ReadingVersion,
            pointer: 0,
            current: "".to_owned(),

            packet_version: 0,
            packet_typeid: 0,
            packet: 0,
        }
    }

    fn receive(self: &mut Self, input: Input) {
        match self.state {
            ReadingVersion => self.read_version(input),
            ReadingTypeId => self.read_typeid(input),
            ReadingNumber => self.read_number(input),
            ReadingLastNumber => self.read_last_number(input),
            Done => (),
        }
    }

    fn read_version(self: &mut Self, input: Input) {
        self.current.push(input.0);
        self.pointer += 1;

        if self.pointer == 3 {
            self.state = ReadingTypeId;
            self.pointer = 0;
            self.packet_version = parse_bin_string(&self.current);
            self.current.clear();
        }
    }

    fn read_typeid(self: &mut Self, input: Input) {
        self.current.push(input.0);
        self.pointer += 1;

        if self.pointer == 3 {
            self.state = ReadingNumber;
            self.pointer = 0;
            self.packet_typeid = parse_bin_string(&self.current);
            self.current.clear();
        }
    }

    fn read_number(self: &mut Self, input: Input) {
        if self.pointer == 0 && input.0 == '1' {
            self.pointer += 1;
        } else if self.pointer == 0 && input.0 == '0' {
            self.pointer += 1;
            self.state = ReadingLastNumber;
        } else {
            self.current.push(input.0);
            self.pointer += 1;

            if self.pointer == 5 {
                self.pointer = 0;
                let number = parse_bin_string(&self.current);
                self.packet <<= 4;
                self.packet += number;
                self.current.clear();
            }
        }
    }

    fn read_last_number(self: &mut Self, input: Input) {
        self.current.push(input.0);
        self.pointer += 1;

        if self.pointer == 5 {
            self.pointer = 0;
            let number = parse_bin_string(&self.current);
            self.packet <<= 4;
            self.packet += number;
            self.state = Done;
        }
    }
}

fn parse_bin_string(binstr: &str) -> u64 {
    let mut result = 0;
    for ch in binstr.chars() {
        result <<= 1;
        if ch == '1' {
            result += 1;
        }
    }
    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_deci_to_bin() {
        // Just test a few examples
        let result = decimal_to_binary(0);
        assert_eq!(result, "0000");
        let result = decimal_to_binary(1);
        assert_eq!(result, "0001");
        let result = decimal_to_binary(4);
        assert_eq!(result, "0100");
        let result = decimal_to_binary(6);
        assert_eq!(result, "0110");
        let result = decimal_to_binary(15);
        assert_eq!(result, "1111");
        let result = decimal_to_binary(12);
        assert_eq!(result, "1100");
    }

    #[test]
    fn test_parse_bin() {
        let result = parse_bin_string("0010");
        assert_eq!(result, 2);
        let result = parse_bin_string("0011");
        assert_eq!(result, 3);
        let result = parse_bin_string("0110");
        assert_eq!(result, 6);
        let result = parse_bin_string("0111");
        assert_eq!(result, 7);
        let result = parse_bin_string("1010");
        assert_eq!(result, 10);
        let result = parse_bin_string("1110");
        assert_eq!(result, 14);
        let result = parse_bin_string("10010");
        assert_eq!(result, 18);
        let result = parse_bin_string("100010");
        assert_eq!(result, 34);
    }

    #[test]
    fn test_fsm_reads_string() {
        let test_str = "110100101111111000101000";
        let mut fsm = FSMDataReader::new();
        for ch in test_str.chars() {
            fsm.receive(Input(ch));
        }

        assert_eq!(fsm.packet_version, 6);
        assert_eq!(fsm.packet_typeid, 4);
        assert_eq!(fsm.packet, 2021);
    }
}
