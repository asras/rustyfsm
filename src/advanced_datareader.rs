// See https://adventofcode.com/2021/day/16 for context
// We implement a (generalized) FSM for parsing the binary data strings
// in the AOC problem

pub fn read_data(contents: &str) -> String {
    //let contents = std::fs::read_to_string("input.txt").expect("Couldnt read file");

    let mut bit_contents = "".to_owned();

    for ch in contents.chars() {
        let mut charnum = ch as u8 - '0' as u8;
        if charnum > 10 {
            charnum -= 7;
        }
        bit_contents.push_str(&decimal_to_binary(charnum));
    }

    bit_contents
}

pub fn decimal_to_binary(num: u8) -> String {
    let mut result = "".to_string();

    for i in 0..4 {
        if ((num >> i) & 1) != 0 {
            result.push_str("1");
        } else {
            result.push_str("0");
        }
    }

    result.chars().rev().collect()
}

pub fn parse_bin_string(binstr: &str) -> u64 {
    let mut result = 0;
    for ch in binstr.chars() {
        result <<= 1;
        if ch == '1' {
            result += 1;
        }
    }
    result
}

#[derive(Copy, Clone, Debug)]
pub struct Input(char);

#[derive(Copy, Clone, Debug)]
pub enum LengthInfo {
    Bits(u64),
    Packets(u64),
}
use LengthInfo::*;

#[derive(Copy, Clone, PartialEq, Debug)]
pub enum OperatorType {
    Sum,
    Product,
    Minimum,
    Maximum,
    GreaterThan,
    LessThan,
    EqualTo,
}
use OperatorType::*;

impl OperatorType {
    pub fn from(type_id: u64) -> Self {
        match type_id {
            0 => Sum,
            1 => Product,
            2 => Minimum,
            3 => Maximum,
            4 => panic!("This is not an operator type!"),
            5 => GreaterThan,
            6 => LessThan,
            7 => EqualTo,
            _ => panic!("Unknown type id {}", type_id),
        }
    }

    pub fn to_typeid(self: Self) -> u64 {
        match self {
            Sum => 0,
            Product => 1,
            Minimum => 2,
            Maximum => 3,
            GreaterThan => 5,
            LessThan => 6,
            EqualTo => 7,
        }
    }
}

#[derive(Debug)]
pub enum Packet {
    Number(u64, u64),
    Operator {
        op_type: OperatorType,
        length: LengthInfo,
        subpackets: Vec<Packet>,
    },
}
use Packet::*;

impl Packet {
    pub fn bit_length(self: &Self) -> u64 {
        match self {
            Number(_, len) => *len,
            Operator {
                op_type: _,
                length,
                subpackets,
            } => {
                let header_length = match length {
                    Bits(_) => 15,
                    Packets(_) => 11,
                };
                let header_length = header_length + 7 as u64;
                header_length + subpackets.iter().map(|p| p.bit_length()).sum::<u64>()
            }
        }
    }

    pub fn number_val(self: &Self) -> u64 {
        match self {
            Number(val, _) => *val,
            _ => panic!("Invalid cast"),
        }
    }
}

pub enum FSMState {
    ReadingVersion,
    ReadingTypeId,
    ReadingLengthTypeId,
    ReadingBitsLength,
    ReadingPacketsLength,
    ReadingNumber,
    ReadingLastNumber,
    Done,
}
use FSMState::*;

pub struct FSMDataReader {
    state: FSMState,
    packet_version: u64,
    pointer: u64,
    current: String,
    packet_stack: Vec<Packet>,
    value: Option<Packet>,

    trace: bool,
}

impl FSMDataReader {
    pub fn new() -> Self {
        Self {
            state: ReadingVersion,
            packet_version: 0,
            pointer: 0,
            current: "".to_owned(),
            packet_stack: vec![],
            value: None,

            trace: false,
        }
    }

    pub fn receive(self: &mut Self, input: Input) {
        match self.state {
            ReadingVersion => self.read_version(input),
            ReadingTypeId => self.read_type_id(input),
            ReadingLengthTypeId => self.read_length_type_id(input),
            ReadingNumber => self.read_number(input),
            ReadingLastNumber => self.read_last_number(input),
            ReadingBitsLength => self.read_bits_length(input),
            ReadingPacketsLength => self.read_packets_length(input),
            Done => (),
        }
    }

    pub fn tracing_receive(self: &mut Self, input: Input) {
        match self.state {
            ReadingVersion => {
                println!("ReadingVersion");
                self.read_version(input);
            }
            ReadingTypeId => {
                println!("ReadingTypeId");
                self.read_type_id(input);
            }
            ReadingLengthTypeId => {
                println!("ReadingLengthTypeId");
                self.read_length_type_id(input);
            }
            ReadingNumber => {
                println!("ReadingNumber");
                self.read_number(input);
            }
            ReadingLastNumber => {
                println!("ReadingLastNumber");
                self.read_last_number(input);
            }
            ReadingBitsLength => {
                println!("ReadingBitsLength");
                self.read_bits_length(input);
            }
            ReadingPacketsLength => {
                println!("ReadingPacketsLength");
                self.read_packets_length(input);
            }
            Done => println!("Done"),
        }
    }

    pub fn read_version(self: &mut Self, input: Input) {
        self.current.push(input.0);
        self.pointer += 1;

        if self.pointer == 3 {
            self.state = ReadingTypeId;
            self.pointer = 0;
            self.packet_version = parse_bin_string(&self.current);
            if self.trace {
                println!(
                    "FOUND VERSION {} FROM STR {}",
                    self.packet_version, self.current
                );
            }
            self.current.clear();
        }
    }

    pub fn read_type_id(self: &mut Self, input: Input) {
        self.current.push(input.0);
        self.pointer += 1;

        if self.pointer == 3 {
            self.pointer = 0;
            let packet_typeid = parse_bin_string(&self.current);
            if self.trace {
                println!(
                    "FOUND TYPEID {} FROM STR {}",
                    packet_typeid, self.current
                );
            }

            match packet_typeid {
                // Just a number packet
                4 => {
                    self.init_number_packet();
                    self.state = ReadingNumber;
                }
                // An operator packet, we need to read the length type
                _ => {
                    self.init_operator_packet(packet_typeid);
                    self.state = ReadingLengthTypeId;
                }
            }

            self.current.clear();
        }
    }

    pub fn read_length_type_id(self: &mut Self, input: Input) {
        match input.0 {
            '0' => {
                self.state = ReadingBitsLength;
            }
            '1' => {
                self.state = ReadingPacketsLength;
            }
            _ => panic!("Unknown length type id: {}", input.0),
        };

        if self.trace {
            println!(
                "FOUND LENGTH TYPE {}",
                input.0
            );
        }
    }

    pub fn read_number(self: &mut Self, input: Input) {
        self.pointer += 1;
        if self.pointer == 1 && input.0 == '1' {
            return;
        } else if self.pointer == 1 && input.0 == '0' {
            self.state = ReadingLastNumber;
            return;
        } else {
            self.current.push(input.0);

            if self.pointer == 5 {
                self.pointer = 0;
                let number = parse_bin_string(&self.current);
                self.increment_number(number);
                self.current.clear();
            }
        }
    }

    fn read_last_number(self: &mut Self, input: Input) {
        self.current.push(input.0);
        self.pointer += 1;

        if self.pointer == 5 {
            self.pointer = 0;
            let number = parse_bin_string(&self.current);
            self.increment_number(number);
            self.handle_packet_done();
            self.state = ReadingVersion;
            self.current.clear();
        }
    }

    pub fn increment_number(self: &mut Self, value: u64) {
        let stack_len = self.packet_stack.len();
        let last_packet = &mut self.packet_stack[stack_len - 1];
        match last_packet {
            Number(x, l) => {
                *x <<= 4;
                *x += value;
                *l += 4;
            }
            _ => panic!("Wrong type"),
        }
    }

    pub fn read_bits_length(self: &mut Self, input: Input) {
        self.pointer += 1;
        self.current.push(input.0);

        if self.pointer == 15 {
            let len = parse_bin_string(&self.current);
            // Update current packet length info
            if self.trace {
                println!("FOUND BITS LEN {} FROM STR {}", len, self.current);
            }
            self.set_length_info(Bits(len));
            self.pointer = 0;
            self.state = ReadingVersion;
            self.current.clear();
        }
    }

    pub fn set_length_info(self: &mut Self, len: LengthInfo) {
        let stack_len = self.packet_stack.len();
        let last_packet = &mut self.packet_stack[stack_len - 1];
        match last_packet {
            Operator {
                op_type: _,
                length,
                subpackets: _,
            } => *length = len,
            _ => panic!("Wrong type"),
        }
    }

    pub fn read_packets_length(self: &mut Self, input: Input) {
        self.pointer += 1;
        self.current.push(input.0);

        if self.pointer == 11 {
            let len = parse_bin_string(&self.current);
            // Update current packet length info
            self.set_length_info(Packets(len));
            self.pointer = 0;
            self.state = ReadingVersion;
            if self.trace {
                println!("FOUND PACKET LEN {} FROM STR {}", len, self.current);
            }
            self.current.clear();
        }
    }

    pub fn init_operator_packet(self: &mut Self, packet_typeid: u64) {
        let current_packet = Operator {
            op_type: OperatorType::from(packet_typeid),
            length: Bits(0),
            subpackets: vec![],
        };
        self.packet_stack.push(current_packet);
    }

    pub fn init_number_packet(self: &mut Self) {
        let num_packet = Number(0, 7);
        self.packet_stack.push(num_packet);
    }

    pub fn handle_packet_done(self: &mut Self) {
        let packet = self.packet_stack.pop().expect("Stack underflow");

        self.add_packet(packet);
    }

    pub fn add_packet(self: &mut Self, packet: Packet) {
        // Put input packet into previous packet
        let stack_len = self.packet_stack.len();
        if self.trace {
            println!("Stack len {}", stack_len);
        }
        if stack_len == 0 {
            self.value = Some(packet);
            self.state = Done;
            return;
        }

        let prev_packet = &mut self.packet_stack[stack_len - 1];
        if self.trace {
            println!("\nAdding packet\n{:?}\nto {:?}\n", packet, prev_packet);
        }
        let need_more;
        match prev_packet {
            Operator {
                op_type: _,
                length,
                subpackets,
            } => {
                (*subpackets).push(packet);
                need_more = match length {
                    Bits(x) => *x > (*subpackets).iter().map(|p| p.bit_length()).sum(),
                    Packets(x) => *x > subpackets.len() as u64,
                };
            }
            _ => panic!("Wrong type"),
        }

        if need_more {
            self.state = ReadingVersion;
        } else {
            // The previous packet is now done
            let prev_packet = self.packet_stack.pop().unwrap();
            // and therefore it should be added to the prev packet
            self.add_packet(prev_packet);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_deci_to_bin() {
        // Just test a few examples
        let result = decimal_to_binary(0);
        assert_eq!(result, "0000");
        let result = decimal_to_binary(1);
        assert_eq!(result, "0001");
        let result = decimal_to_binary(4);
        assert_eq!(result, "0100");
        let result = decimal_to_binary(6);
        assert_eq!(result, "0110");
        let result = decimal_to_binary(15);
        assert_eq!(result, "1111");
        let result = decimal_to_binary(12);
        assert_eq!(result, "1100");
    }

    #[test]
    fn test_parse_bin() {
        let result = parse_bin_string("0010");
        assert_eq!(result, 2);
        let result = parse_bin_string("0011");
        assert_eq!(result, 3);
        let result = parse_bin_string("0110");
        assert_eq!(result, 6);
        let result = parse_bin_string("0111");
        assert_eq!(result, 7);
        let result = parse_bin_string("1010");
        assert_eq!(result, 10);
        let result = parse_bin_string("1110");
        assert_eq!(result, 14);
        let result = parse_bin_string("10010");
        assert_eq!(result, 18);
        let result = parse_bin_string("100010");
        assert_eq!(result, 34);
    }

    #[test]
    fn test_fsm_reads_single_packet() {
        let test_str = "110100101111111000101000";
        let mut fsm = FSMDataReader::new();
        for ch in test_str.chars() {
            fsm.receive(Input(ch));
        }

        let packet = fsm.value.unwrap();
        match packet {
            Number(value, len) => {
                assert_eq!(value, 2021);
                assert_eq!(len, 18);
            }
            _ => panic!("Wrong type in test"),
        };
        assert_eq!(fsm.packet_stack.len(), 0);
    }

    #[test]
    fn test_read_hex() {
        let hex = "38006F45291200";
        let out = read_data(hex);
        let exp = "00111000000000000110111101000101001010010001001000000000";

        assert_eq!(out, exp);
    }

    #[test]
    fn test_fsm_reads_sum_packet() {
        let hex_data = "C200B40A82";
        let test_str = read_data(hex_data);
        // println!("{}", test_str);
        // println!("123456789 123456789 123456789 123456789 ");
        let mut fsm = FSMDataReader::new();
        for ch in test_str.chars() {
            fsm.receive(Input(ch));
        }
        let packet = fsm.value.unwrap();
        // println!("{:?}", packet);
        match packet {
            Operator {
                op_type,
                length: _,
                subpackets,
            } => {
                assert_eq!(op_type, Sum);
                assert_eq!(subpackets.len(), 2);
                assert_eq!(subpackets.iter().map(|p| p.number_val()).sum::<u64>(), 3);
            }
            _ => panic!("Wrong type in test"),
        };

        assert_eq!(fsm.packet_stack.len(), 0);
    }

    #[test]
    fn test_fsm_reads_product_packet() {
        let hex_data = "04005AC33890";
        let test_str = read_data(hex_data);
        let mut fsm = FSMDataReader::new();

        for ch in test_str.chars() {
            fsm.receive(Input(ch));
        }
        let packet = fsm.value.expect("Reader did not produce a final output");

        match packet {
            Operator {
                op_type,
                length: _,
                subpackets,
            } => {
                assert_eq!(op_type, Product);
                assert_eq!(subpackets.len(), 2);
                assert_eq!(
                    subpackets.iter().map(|p| p.number_val()).product::<u64>(),
                    54
                );
            }
            _ => panic!("Wrong packet type"),
        };
        assert_eq!(fsm.packet_stack.len(), 0);
    }

    #[test]
    fn test_fsm_reads_min_packet() {
        todo!()
    }

    #[test]
    fn test_fsm_reads_max_packet() {
        todo!()
    }

    #[test]
    fn test_fsm_reads_lessthan_packet() {
        todo!()
    }

    #[test]
    fn test_fsm_reads_greaterthan_packet() {
        todo!()
    }

    #[test]
    fn test_fsm_reads_eq_packet() {
        todo!()
    }
}
